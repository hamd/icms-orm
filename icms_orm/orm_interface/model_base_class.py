from typing import Any, Dict, List, Optional, Type, Union
from sqlalchemy import orm
from sqlalchemy.orm.query import Query
from sqlalchemy.orm.scoping import scoped_session
from sqlalchemy.sql.schema import Column, MetaData, Table
import stringcase
import sqlalchemy as sa
from collections import OrderedDict
from sqlalchemy.orm.attributes import InstrumentedAttribute
from sqlalchemy.orm.relationships import RelationshipProperty
from sqlalchemy.orm import ColumnProperty


class IcmsModelBase(object):
    """
    Base for all our mapper classes (through bind-specific mixins).
    Exposes reusable methods as well as a part of the protocol imposed by flask-sqlalchemy/alchy (query, session)
    """

    class QueryDescriptor:
        def __get__(self, obj, objtype=None) -> Query:
            return IcmsModelBase.session().query(orm.class_mapper(objtype))

    class SessionDescriptor:
        __session: Optional[scoped_session] = None

        def __get__(self, obj, objtype=None) -> Optional[scoped_session]:
            return IcmsModelBase.SessionDescriptor.__session

        def __set__(self, obj, value: scoped_session):
            assert IcmsModelBase.SessionDescriptor.__session is None, 'This descriptor cannot be re-initialized. Perhaps there are multiple instances of OrmManager?'
            IcmsModelBase.SessionDescriptor.__session = value

    class TablenameDescriptor:
        def __get__(self, obj, objtype: Optional[Type] = None) -> str:
            return stringcase.snakecase(objtype.__name__)

    query: QueryDescriptor = QueryDescriptor()
    session: SessionDescriptor = SessionDescriptor()
    # Pre-setting some defaults / empty values for type-assistance. 
    metadata: Optional[MetaData] = None
    __table__: Optional[Table] = None
    __tablename__ = TablenameDescriptor()

    def __str__(self) -> str:
        details = []
        for attr_name, attr_value in self.to_dict().items():
            details.append('%s=%s' % (attr_name, attr_value))
        result = f'<{self.__class__.__name__}:{";".join(details)}>'
        return result

    def to_dict(self) -> Dict[str, Any]:
        result = OrderedDict()
        for ia in self.ia_list():
            result[ia.key] = self.get(ia)
        return result

    def to_ia_dict(self) -> Dict[Union[Column, InstrumentedAttribute], Any]:
        result = dict()
        for ia in self.ia_list():
            result[ia] = self.get(ia)
        return result

    @classmethod
    def from_ia_dict(cls: Type['IcmsModelBase'], ia_dict: Optional[Dict[Union[InstrumentedAttribute, Column], Any]] = None):
        """
        Constructs a mapper object from a dictionary where keys are SQLAlchemy's instrumented attributes
        :param ia_dict: a dictionary with SA instrumented attributes as keys
        :return:
        """
        ia_dict = ia_dict or dict()
        instance = cls()
        expression_dict = {ia.expression: value for ia, value in ia_dict.items()}

        for ia in cls.relationships():
            if ia in ia_dict.keys():
                instance.set(ia, ia_dict.get(ia))

        for ia in cls.ia_list():
            expression = ia.expression
            if expression in expression_dict.keys():
                value = expression_dict.get(expression)
                instance.set(ia, value)
            elif expression.foreign_keys:
                ref_column = next(iter(expression.foreign_keys)).column
                if ref_column in expression_dict.keys():
                    instance.set(ia, expression_dict.get(ref_column))
        return instance

    @classmethod
    def ia_list(cls: Type['IcmsModelBase']) -> List[InstrumentedAttribute]:
        """
        :return: A list of instrumented attributes representing the columns of the mapped table
        """
        return [c for c in cls.__dict__.values() if isinstance(c, InstrumentedAttribute) and isinstance(c.prop, ColumnProperty)]

    @classmethod
    def relationships(cls: Type['IcmsModelBase']) -> List[InstrumentedAttribute]:
        """
        :return A list of instrumented attributes representing relationships defined for the mapped table
        """
        return [c for c in cls.__dict__.values() if isinstance(c, InstrumentedAttribute) and isinstance(c.prop, RelationshipProperty)]

    @classmethod
    def primary_keys(cls: Type['IcmsModelBase']) -> List[InstrumentedAttribute]:
        return [ia for ia in cls.ia_list() if ia.expression.primary_key]

    def get(self, ia: InstrumentedAttribute) -> Any:
        return getattr(self, ia.key)

    def set(self, ia: InstrumentedAttribute, value: Any):
        setattr(self, ia.key, value)

    @classmethod
    def add_unique_key(cls, key_name, *cols_list):
        # starting out with some default that is expected to raise exceptions if we end up with sth else than handled...
        table_args = cls.__table_args__
        if isinstance(table_args, dict):
            # __table_args__ is a dict unless it's not - then it's an elegant list of small dicts and other junk
            table_args = [{_k: _v} for _k, _v in table_args.items()]
        assert isinstance(table_args, list)
        table_args.append(sa.UniqueConstraint(*cols_list, name=key_name))
        cls.__table_args__ = table_args
