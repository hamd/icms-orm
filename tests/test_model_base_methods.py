import pytest
from icms_orm import IcmsDeclarativeBasesFactory
import sqlalchemy as sa


class MockDeclarativeBasesFactory(IcmsDeclarativeBasesFactory):
    pass


@pytest.fixture(scope='module')
def define_mappers():
    declarative_base = MockDeclarativeBasesFactory.get_for_bind_key('testbind')

    class Planet(declarative_base):
        id = sa.Column(sa.Integer, primary_key=True)
        name = sa.Column(sa.String(64), unique=True)

    class Moon(declarative_base):
        id = sa.Column(sa.Integer, primary_key=True)
        name = sa.Column(sa.String(64))
        planet_name = sa.Column(sa.String(64), sa.ForeignKey(Planet.name))
        planet = sa.orm.relationship(Planet, foreign_keys=[planet_name])

    return Planet, Moon


class TestInstanceAssembly(object):

    test_data = [('Jupiter', 'Ganymede'), ('Saturn', 'Enceladus'),
                 ('Jupiter', 'Europa'), ('Neptune', 'Triton')]

    @pytest.mark.parametrize("planet_name, moon_name", test_data)
    def test_basic_assembly(self, define_mappers, planet_name, moon_name):
        """
        Test aims to verify that parameters from the dictionary are correctly assigned to the entity of corresponding type.
        Construction should also identify foreign key values within the dictionary and use them if available.
        """
        (Planet, Moon) = define_mappers
        data = {Planet.name: planet_name, Moon.name: moon_name}
        planet = Planet.from_ia_dict(data)
        moon = Moon.from_ia_dict(data)
        assert planet.name == planet_name, 'Planet\'s name «{0}» should have been picked up rather than «{1}»'.format(
            planet_name, planet.name)
        assert moon.name == moon_name, 'Moon\'s name «{0}» should have been picked up rather than «{1}»'.format(
            moon_name, moon.name)

    @pytest.mark.parametrize("planet_name, moon_name", test_data)
    def test_referenced_column_values_are_picked_up(self, define_mappers, planet_name, moon_name):
        (Planet, Moon) = define_mappers
        data = {Planet.name: planet_name, Moon.name: moon_name}
        moon = Moon.from_ia_dict(data)
        assert moon.planet_name == planet_name, 'Planet\'s name «{0}» should have been discovered through FK relationship rather than «{1}»'.format(
            planet_name, moon.planet_name)

    @pytest.mark.parametrize("planet_name, moon_name", test_data)
    def test_assembly_through_relationship_assignment(self, define_mappers, planet_name, moon_name):
        (Planet, Moon) = define_mappers
        planet = Planet.from_ia_dict({Planet.name: planet_name})
        moon = Moon.from_ia_dict({Moon.name: moon_name, Moon.planet: planet})
        assert moon.planet is not None
        assert moon.planet.name == planet_name, 'Planet\'s name «{0}» should have been discovered through the relationship setup but found «{1}»'.format(
            planet_name, moon.planet_name)


class TestGetters(object):
    def test_mappers_structure(self, define_mappers):
        (Planet, Moon) = define_mappers
        assert Planet is not None
        assert Planet.__table__ is not None
        assert Planet.__table__.metadata is not None

        assert Moon is not None
        assert Moon.__table__ is not None
        assert Moon.__table__.metadata is not None

    def test_ia_list(self, define_mappers):
        (Planet, Moon) = define_mappers
        assert 2 == len(Planet.ia_list())
        assert {Planet.id, Planet.name} == set(Planet.ia_list())
        assert 3 == len(Moon.ia_list())
        assert {Moon.id, Moon.name, Moon.planet_name} == set(Moon.ia_list())

    def test_relationships(self, define_mappers):
        (Planet, Moon) = define_mappers
        assert 1 == len(Moon.relationships())
        assert {Moon.planet} == set(Moon.relationships())
        assert 0 == len(Planet.relationships())