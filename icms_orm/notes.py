#  Copyright (c) 2015-2019 CERN, Geneva, Switzerland
#  SPDX-License-Identifier: Apache-2.0

from typing import Type
from icms_orm.orm_interface.model_base_class import IcmsModelBase
from sqlalchemy import Column, ForeignKey
from sqlalchemy import Sequence
from sqlalchemy import Table
from sqlalchemy.orm import relationship, backref
from sqlalchemy.types import String, Integer, Text, Boolean, DateTime
from sqlalchemy.dialects.postgresql import JSONB
from icms_orm import notes_bind_key, notes_schema_name, IcmsDeclarativeBasesFactory
from datetime import datetime


DeclarativeBase: Type[IcmsModelBase] = IcmsDeclarativeBasesFactory.get_for_bind_key(notes_bind_key() or '')


class NotesBaseMixin():
    __bind_key__ = notes_bind_key()
    __table_args__ = {'schema': notes_schema_name()}


class NoteType(NotesBaseMixin, DeclarativeBase):
    __tablename__ = 'notetypes'

    id = Column(Integer, Sequence('id_seq_ntype'), primary_key=True)
    type = Column(String(4), nullable=False, unique=True)
    type_description = Column(String(30), nullable=False)
    is_usable = Column(Boolean, nullable=False)

    def __init__(self, note_type=None, type_description=None, is_usable=1):
        self.type = note_type
        self.type_description = type_description
        self.is_usable = is_usable


class Note(NotesBaseMixin, DeclarativeBase):
    __tablename__ = 'notes'

    id = Column(Integer, Sequence('id_seq_note'), primary_key=True, nullable=False)
    type_id = Column(ForeignKey(NoteType.id), nullable=False, index=True)
    code = Column(String(36))
    cms_note_id = Column(String(17), unique=True)
    title = Column(String(4000), nullable=False)
    abstract = Column(Text, nullable=True)
    subproject = Column(String(255))
    subdetector = Column(String(255))
    working_group = Column(String(255))
    vars = Column('vars', JSONB)

    type = relationship(NoteType)

    def __init__(self, note_type, code, title,
                 abstract=None,
                 subproject=None,
                 cms_note_id=None,
                 subdetector=None,
                 note_id=None,
                 working_group=None,
                 vars=None):
        self.id = note_id
        self.type_id = note_type
        self.code = code
        self.cms_note_id = cms_note_id
        self.title = title
        self.abstract = abstract
        self.subproject = subproject
        self.subdetector = subdetector
        self.working_group = working_group
        self.vars = vars


class Actor(NotesBaseMixin, DeclarativeBase):
    __tablename__ = 'actors'

    id = Column(Integer, Sequence('id_seq_actr'), primary_key=True, nullable=False)
    note_id = Column(ForeignKey(Note.id), nullable=False, index=True)
    cms_id = Column(Integer)
    hr_id = Column(Integer)
    role = Column(String(45), nullable=False)
    status = Column(String(45), nullable=False)
    timestamp = Column(DateTime, nullable=False, default=datetime.utcnow)
    external_id = Column(String(100))

    note = relationship(Note)

    def __init__(self, note_id, role=None, status=None, cms_id=0, hr_id=0, external_id=None, timestamp=None):
        self.note_id = note_id
        self.cms_id = cms_id
        self.hr_id = hr_id
        self.role = role
        self.status = status
        self.external_id = external_id
        self.timestamp = timestamp


class Category(NotesBaseMixin, DeclarativeBase):
    __tablename__ = 'categories'

    id = Column(Integer, Sequence('id_seq_cat'), primary_key=True, nullable=False)
    short_name = Column(String(20), nullable=False)
    parent_id = None
    category_name = Column(String(50), nullable=False)
    is_usable = Column(Integer)

    def __init__(self, short_name, category_name, parent_id=None, is_usable=True):
        self.short_name = short_name
        self.parent_id = parent_id
        self.category_name = category_name
        self.is_usable = is_usable
Category.parent_id = Column(ForeignKey(Category.id), nullable=True)


class CategoriesRel(NotesBaseMixin, DeclarativeBase):
    __tablename__ = 'categoriesrel'

    id = Column(Integer, Sequence('id_seq_catrel'), primary_key=True, nullable=False)
    note_id = Column(ForeignKey(Note.id), nullable=False, index=True)
    category_id = Column(ForeignKey(Category.id), nullable=False, index=True)

    category = relationship(Category)
    note = relationship(Note)

    def __init__(self, note_id, category_id):
        self.note_id = note_id
        self.category_id = category_id


class File(NotesBaseMixin, DeclarativeBase):
    __tablename__ = 'files'

    id = Column(Integer, Sequence('id_seq_file'), primary_key=True, nullable=False)
    note_id = Column(ForeignKey(Note.id), nullable=False, index=True)
    file_name = Column(String(255), nullable=False)
    modification_date = Column(DateTime, nullable=False, default=datetime.utcnow)
    link = Column(String(255))

    note = relationship(Note)

    def __init__(self, note_id, file_name, modification_date=None, link=None):
        self.note_id = note_id
        self.file_name = file_name
        self.modification_date = modification_date
        self.link = link


class Process(NotesBaseMixin, DeclarativeBase):
    __tablename__ = 'processes'

    id = Column(Integer, Sequence('id_seq_proc'), primary_key=True, nullable=False)
    note_id = Column(ForeignKey(Note.id), nullable=False, index=True)
    start_date = Column(DateTime, nullable=False, default=datetime(1990, 1, 1))
    modification_date = Column(DateTime, nullable=False, default=datetime(1990, 1, 1))
    end_date = Column(DateTime, nullable=False, default=datetime(1990, 1, 1))
    status = Column(String(255), nullable=False)
    is_redirectable = Column(Boolean)
    frozen_after_submit = Column(Boolean)

    note = relationship(Note, backref=backref('processes', lazy='joined'))

    def __init__(self, note_id,
                 start_date=None,
                 modification_date=None,
                 end_date=None,
                 status=None,
                 is_redirectable=None,
                 frozen_after_submit=True):
        self.note_id = note_id
        self.start_date = start_date
        self.end_date = end_date
        self.status = status
        self.modification_date = modification_date
        self.is_redirectable = is_redirectable
        self.frozen_after_submit = frozen_after_submit


class NoteRemark(NotesBaseMixin, DeclarativeBase):
    __tablename__ = 'noteremarks'

    id = Column(Integer, Sequence('id_seq_nrem'), primary_key=True, nullable=False)
    process_id = Column(ForeignKey(Process.id), nullable=False, index=True)
    date = Column(DateTime, nullable=False, default=datetime(1990, 1, 1))
    completed = Column(Boolean)
    remark = Column(Text)
    actor_id = Column(ForeignKey(Actor.id), nullable=True)
    related_to = None
    process = relationship(Process)

    def __init__(self, process_id=None, date=None, completed=None, remark=None, actor_id=None,
                 related_to=None):
        self.process_id = process_id
        self.date = date
        self.completed = completed
        self.remark = remark
        self.actor_id = actor_id
        self.related_to = related_to

NoteRemark.related_to = Column(ForeignKey(NoteRemark.id), nullable=True, index=True)


class WorkflowRecord(NotesBaseMixin, DeclarativeBase):
    __tablename__ = 'workflowrecords'

    id = Column(Integer, Sequence('id_seq_wflr'), primary_key=True, nullable=False)
    process_id = Column(ForeignKey(Process.id), nullable=False, index=True)
    actor_id = Column(ForeignKey(Actor.id), nullable=False, index=True)
    date = Column(DateTime, nullable=False, default=datetime(1990, 1, 1))
    diff = Column(Text, nullable=True)
    action = Column(String(255))

    process = relationship(Process)
    actor = relationship(Actor)

    def __init__(self, process_id, actor_id, action, date=None, diff=None):
        self.process_id = process_id
        self.actor_id = actor_id
        self.date = date
        self.diff = diff
        self.action = action


t_notewithactorandtype = Table(
    'notewithactorandtype', DeclarativeBase.metadata,
    Column('status', String(255)),
    Column('cms_id', Integer),
    Column('hr_id', Integer),
    Column('role', String(45)),
    Column('actor_status', String(45)),
    Column('external_id', String(100)),
    Column('id', Integer),
    Column('cms_note_id', String(17)),
    Column('title', String(4000)),
    Column('abstract', Text),
    Column('modification_date', DateTime, default=datetime(1990, 1, 1)),
    Column('type', String(4)),
    Column('type_description', String(30)),
    Column('subproject', String(255)),
    Column('subdetector', String(255)),
    Column('working_group', String(255))
)
