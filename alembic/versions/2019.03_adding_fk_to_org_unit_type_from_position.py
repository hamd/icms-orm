"""Adding FK to org_unit_type from position

Revision ID: 2019.03
Revises: 2019.02
Create Date: 2019-04-04 17:05:43.972379

"""
from alembic import op
import sqlalchemy as sa
from sqlalchemy.dialects import postgresql

# revision identifiers, used by Alembic.
revision = '2019.03'
down_revision = '2019.02'
branch_labels = None
depends_on = None


def upgrade():
    op.add_column('position', sa.Column('unit_type_id', sa.SmallInteger(), nullable=False))
    op.create_foreign_key(op.f('fk_position_unit_type_id_org_unit_type'), 'position', 'org_unit_type', ['unit_type_id'], ['id'], source_schema='public', referent_schema='public', onupdate='CASCADE', ondelete='CASCADE')


def downgrade():
    op.drop_constraint(op.f('fk_position_unit_type_id_org_unit_type'), 'position', schema='public', type_='foreignkey')
    op.drop_column('position', 'unit_type_id')