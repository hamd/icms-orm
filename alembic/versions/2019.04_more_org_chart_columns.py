"""More org chart columns

Revision ID: 2019.04
Revises: 2019.03
Create Date: 2019-06-06 10:41:09.406021

"""
from alembic import op
import sqlalchemy as sa
from sqlalchemy.dialects import postgresql

# revision identifiers, used by Alembic.
revision = '2019.04'
down_revision = '2019.03'
branch_labels = None
depends_on = None


def upgrade():
    # First - set up the enclosing units
    op.add_column(u'org_unit', sa.Column('enclosing_unit_id', sa.SmallInteger(), nullable=True))
    op.add_column(u'org_unit', sa.Column('outermost_unit_id', sa.SmallInteger(), nullable=True))
    op.create_foreign_key(op.f('fk_org_unit_enclosing_unit_id_org_unit'), 'org_unit', 'org_unit', ['enclosing_unit_id'],
                          ['id'], source_schema='public', referent_schema='public', onupdate='CASCADE',
                          ondelete='CASCADE')
    op.create_foreign_key(op.f('fk_org_unit_outermost_unit_id_org_unit'), 'org_unit', 'org_unit', ['outermost_unit_id'],
                          ['id'], source_schema='public', referent_schema='public', onupdate='CASCADE',
                          ondelete='CASCADE')

    # that columns should just be renamed
    op.alter_column(u'ex_officio_mandate', u'stat_date', new_column_name='start_date', nullable=False)

    # These have to be set as nullable, then updated accordingy and then set as not-nullable (with a default preferably)
    op.add_column(u'org_unit_type', sa.Column('is_enclosed', sa.Boolean(), nullable=True, default=False))
    op.add_column(u'org_unit_type', sa.Column('is_active', sa.Boolean(), nullable=True, default=True))
    op.add_column(u'position', sa.Column('is_active', sa.Boolean(), nullable=True, default=True))
    op.add_column(u'org_unit', sa.Column('is_active', sa.Boolean(), nullable=True, default=True))
    # Now update those above
    op.execute('UPDATE org_unit set is_active=true')
    op.execute('UPDATE org_unit_type set is_enclosed=false')
    op.execute('UPDATE org_unit_type set is_active=true')
    op.execute('UPDATE position set is_active=true')
    # Now set the defaults and disable nulls
    op.alter_column(u'org_unit_type', u'is_enclosed', nullable=False, server_default='false')
    op.alter_column(u'org_unit_type', u'is_active', nullable=False, server_default='true')
    op.alter_column(u'position', u'is_active', nullable=False, server_default='true')
    op.alter_column(u'org_unit', u'is_active', nullable=False, server_default='true')


def downgrade():
    op.drop_constraint(op.f('fk_org_unit_outermost_unit_id_org_unit'), 'org_unit', schema='public', type_='foreignkey')
    op.drop_constraint(op.f('fk_org_unit_enclosing_unit_id_org_unit'), 'org_unit', schema='public', type_='foreignkey')
    op.drop_column(u'org_unit', 'outermost_unit_id')
    op.drop_column(u'org_unit', 'enclosing_unit_id')

    op.drop_column(u'position', 'is_active')
    op.drop_column(u'org_unit_type', 'is_enclosed')
    op.drop_column(u'org_unit_type', 'is_active')
    op.drop_column(u'org_unit', 'is_active')
    op.alter_column(u'ex_officio_mandate', u'start_date', new_column_name='stat_date', nullable=False)
